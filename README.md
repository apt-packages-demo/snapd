# snapd

Daemon and tooling that enable snap packages. https://tracker.debian.org/snapd
* https://packages.ubuntu.com/en/snapd

## Removing snap(d)
* $ snap list # chromium may be installed in it...
  * [*How to remove snap completely without losing the Chromium browser?*
    ](https://askubuntu.com/questions/1179273/how-to-remove-snap-completely-without-losing-the-chromium-browser)
  * See apt... chromium